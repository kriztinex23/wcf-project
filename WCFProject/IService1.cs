﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFProject
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
   
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        Instrument GetInstrument(int id, decimal changePrice);

        //#region Get List From StockDB


        //[OperationContract]
        //Instrument_Type GetInstrumentType(int id);

        //[OperationContract]
        //List<Contract> GetContract(int id);

        //[OperationContract]
        //List<Market> GetMarket(int id);

        //[OperationContract]
        //List<NadexCurrency> GetNadexCurrency(int id);

        //[OperationContract]
        //List<News> GetNews(int id);

        //[OperationContract]
        //List<Periodicity> GetPeriodicity(int id);

        //[OperationContract]
        //List<Position> GetPosition(int id);
        //#endregion

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string Greet(string message);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
