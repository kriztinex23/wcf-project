﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFProject
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    
    //[ServiceBehavior (AddressFilterMode = AddressFilterMode.Any)]

    public class Service1 : EntitiesController , IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }




        public Instrument GetInstrument(int id, decimal changePrice)
        {
            return findInstrumentEntities(id, changePrice);
        }

        #region Get List From StockDB

        public Instrument_Type GetInstrumentType(int id)
        {
            //return new List<Instrument_Type>();
            return findInstrumentTypeEntities(id);
        }
       


        public List<Contract> GetContract(int id)
        {
            return findContractEntities(id);
        }

        public List<Market> GetMarket(int id)
        {
            return findMarketEntities(id);
        }

        public List<NadexCurrency> GetNadexCurrency(int id)
        {
            return findNadexCurrencyEntities(id);
        }

        public List<News> GetNews(int id)
        {
            return findNewsEntities(id);
        }

        public List<Periodicity> GetPeriodicity(int id)
        {
            return findPeriodicityEntities(id);
        }

        public List<Position> GetPosition(int id)
        {
            return findPositionEntities(id);
        }
        #endregion


        public string Greet(string message)
        {
            return "Hello : " + message;
        }
    }
}
