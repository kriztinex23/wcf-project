﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;

namespace Task
{
    class BackgroundWorkers
    {
        public static BackgroundWorker worker = new BackgroundWorker();
        public static int progress;
        public static string message;


        public void Worker()
        {
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            string input = Console.ReadLine();

            if (input.ToUpper() == "Y")
            {
                Console.WriteLine("starting...");
                worker.RunWorkerAsync();
            }

            string secondInput = Console.ReadLine();
            if (secondInput.ToUpper() == "N")
            {
                Console.WriteLine("cancel task...");


                if (worker.IsBusy)
                {
                    worker.CancelAsync();
                }

            }


        }

        public static void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                message = "Task Cancelled";
            }
            else if (e.Error != null)
            {
                message = "Error while performing background operation";
            }
            else
            {
                message = "Task Completed";
            }

            //btnStartAsyncOperation.Enabled = true;
            //btnCancel.Enabled = false;
        }

        public static void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress += e.ProgressPercentage;
            Console.WriteLine("progress {0} ...", progress);
            //Console.WriteLine(".");
        }

        public static void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                worker.ReportProgress(i * 100);


                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    worker.ReportProgress(0);
                    return;
                }

            }

            worker.ReportProgress(100);

        }
    }
}
