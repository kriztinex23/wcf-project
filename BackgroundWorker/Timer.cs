﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Timers;


namespace Task
{

    class TimerSettings
    {


        public static BackgroundWorker worker = new BackgroundWorker();
        public static int progress;
        public static string message;
        public int minuteGap = 1;

        public void StartTimer()
        {
            Timer timer = new Timer();
            const int milliseconds = 1000;
            const int seconds = 5;

            int interval = minuteGap * seconds * milliseconds;

            timer.Interval = interval;
            timer.Start();

            timer.Elapsed += new ElapsedEventHandler(Worker);

            Console.ReadLine();
        }

        private void Worker(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss"));
            
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            //string input = Console.ReadLine();

            //if (input.ToUpper() == "Y")
            //{
                Console.WriteLine("starting...");
                if (!worker.IsBusy)
                {
                    worker.RunWorkerAsync();
                }
                else 
                {
                    worker.CancelAsync();
                }

               
            //}


            
        }

        public static void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                message = "Task Cancelled";
            }
            else if (e.Error != null)
            {
                message = "Error while performing background operation";
            }
            else
            {
                message = "Task Completed";
            }

            //btnStartAsyncOperation.Enabled = true;
            //btnCancel.Enabled = false;
        }

        public static void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress += e.ProgressPercentage;
            Console.WriteLine("progress {0} ...", progress);
            //Console.WriteLine(".");
        }

        public static void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //CALL CONTROlleR

        }




    }
}