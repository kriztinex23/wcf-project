﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
    interface IEntities
    {
        List<Contract> findContractEntities(int id);

        Instrument findInstrumentEntities(int id, decimal changePrice);

        Instrument_Type findInstrumentTypeEntities(int id);

        List<Market> findMarketEntities(int id);
        List<NadexCurrency> findNadexCurrencyEntities(int id);
        List<News> findNewsEntities(int id);
        List<Periodicity> findPeriodicityEntities(int id);
        List<Position> findPositionEntities(int id);
    }
}
