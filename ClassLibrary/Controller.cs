﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using TransportModel;

namespace ClassLibrary
{
    public class Controller
    {

        private BackgroundWorker worker;

        public BackgroundWorker BackgroundWorker
        {
            get {
                if (worker == null)
                    worker = new BackgroundWorker();
                return worker;
            }
            
        }
        private int progress;
        private string message;
        private int minuteGap = 1;

        public void UpdatePrice(InstrumentInfo i)
        {
            string url = "http://localhost:1445/Service1.svc/GetInstrument";

            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(url); request.KeepAlive = false;
            //request.ProtocolVersion = HttpVersion.Version10;
            request.Method = "POST";

            //string json = "{ \"changePrice\": 200 , \"id\":1}";
            string json = JsonConvert.SerializeObject(i);

            byte[] postBytes = Encoding.UTF8.GetBytes(json);

            request.ContentType = "application/json; charset=UTF-8";
            //request.Accept = "application/json";
            //request.ContentLength = postBytes.Length;
            Stream requestStream = request.GetRequestStream();

            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }

            Console.WriteLine(result);
        }

        public void StartTimer()
        {
            Console.WriteLine("started timer");
            Timer timer = new Timer();
            const int milliseconds = 1000;
            const int seconds = 5;

            int interval = minuteGap * seconds * milliseconds;

            timer.Interval = interval;
            timer.Start();

            timer.Elapsed += new ElapsedEventHandler(Worker);
        }

        private void Worker(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss"));

            BackgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            BackgroundWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            BackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            BackgroundWorker.WorkerReportsProgress = true;
            BackgroundWorker.WorkerSupportsCancellation = true;

            //string input = Console.ReadLine();

            //if (input.ToUpper() == "Y")
            //{
            Console.WriteLine("starting...");
            if (!BackgroundWorker.IsBusy)
            {
                BackgroundWorker.RunWorkerAsync();
            }
            else
            {
                BackgroundWorker.CancelAsync();
            }


            //}



        }

        public  void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                message = "Task Cancelled";
            }
            else if (e.Error != null)
            {
                message = "Error while performing background operation";
            }
            else
            {
                message = "Task Completed";
            }

            //btnStartAsyncOperation.Enabled = true;
            //btnCancel.Enabled = false;
        }

        public  void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress += e.ProgressPercentage;
            Console.WriteLine("progress {0} ...", progress);
            //Console.WriteLine(".");
        }

        public  void worker_DoWork(object sender, DoWorkEventArgs e)
        {
                InstrumentInfo i = new InstrumentInfo();
                i.id = 1;
                i.changePrice = 500;

                UpdatePrice(i);


        }


    }
}
