﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
    public class EntitiesController : IEntities
    {
        public stockDBEntities databaseEntity = new stockDBEntities();


        public List<Contract> findContractEntities(int id)
        {
            IQueryable<Contract> contract = from table in databaseEntity.Contract
                                            where table.contract_id == id
                                            select table;
            return contract.ToList();
        }

        public Instrument findInstrumentEntities(int id, decimal changePrice)
        {
            IQueryable<Instrument> instruments = from table in databaseEntity.Instrument
                                                 where table.instrument_id == id
                                                 select table;
            //if (changePrice != 0)
            //{
                instruments.Single().instrument_price = changePrice;
                databaseEntity.SaveChanges();
            //}

            return instruments.SingleOrDefault<Instrument>();
        }

        public Instrument_Type findInstrumentTypeEntities(int id)
        {
            IQueryable<Instrument_Type> instrumentTypes = from table in databaseEntity.Instrument_Type
                                                          where table.instrument_type_id == id
                                                          select table;
            return instrumentTypes.SingleOrDefault<Instrument_Type>();
        }

        public List<Market> findMarketEntities(int id)
        {
            IQueryable<Market> market = from table in databaseEntity.Market
                                        where table.market_id == id
                                        select table;
            return market.ToList();
        }

        public List<NadexCurrency> findNadexCurrencyEntities(int id)
        {
            IQueryable<NadexCurrency> nadexCurrency = from table in databaseEntity.NadexCurrency
                                                      where table.nadex_id == id
                                                      select table;
            return nadexCurrency.ToList();
        }

        public List<News> findNewsEntities(int id)
        {
            IQueryable<News> news = from table in databaseEntity.News
                                    where table.news_id == id
                                    select table;
            return news.ToList();
        }

        public List<Periodicity> findPeriodicityEntities(int id)
        {
            IQueryable<Periodicity> periodicity = from table in databaseEntity.Periodicity
                                                  where table.periodicity_id == id
                                                  select table;
            return periodicity.ToList();
        }

        public List<Position> findPositionEntities(int id)
        {
            IQueryable<Position> position = from table in databaseEntity.Position
                                            where table.position_id == id
                                            select table;
            return position.ToList();
        }



    }
}


